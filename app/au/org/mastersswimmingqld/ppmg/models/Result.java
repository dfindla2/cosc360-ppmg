package au.org.mastersswimmingqld.ppmg.models;

/**
 * Handles splits and final time results
 *
 */
public class Result {

    private long id;

    private boolean split;
    private int distance;
    private float seconds;

}
