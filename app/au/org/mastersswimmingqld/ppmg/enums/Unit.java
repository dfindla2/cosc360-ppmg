package au.org.mastersswimmingqld.ppmg.enums;


public enum Unit {
    Metres  ("Metres", "m"),
    Feet    ("Feet", "ft"),
    Yards   ("Yards", "y");

    private final String name;
    private final String abbr;

    Unit (String name, String abbr) {
        this.name = name;
        this.abbr = abbr;
    }

    String getAbbr() {
        return abbr;
    }

    String getName() {
        return name;
    }
}
