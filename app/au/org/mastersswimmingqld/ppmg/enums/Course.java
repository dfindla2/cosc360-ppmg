package au.org.mastersswimmingqld.ppmg.enums;

/**
 * Created by david on 10/09/2016.
 */
public enum Course {
    SCM      ("Short Course", "SC", 25, Unit.Metres),
    LCM      ("Long Course", "LC", 50, Unit.Metres);

    private final String name;
    private final String abbr;
    private final int distance;
    private final Unit unit;

    Course(String name, String abbr, int distance, Unit unit) {
        this.name = name;
        this.abbr = abbr;
        this.distance = distance;
        this.unit = unit;
    }

    String getName() {
        return name;
    }

    String getAbbr() {
        return abbr;
    }

    int getDistance() {
        return distance;
    }

    Unit getUnit() {
        return unit;
    }
}
