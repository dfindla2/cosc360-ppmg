package au.org.mastersswimmingqld.ppmg.models;

import java.util.List;

/**
 * Handles events in entries
 */
public class EntryEvent {

    private long id;
    private long entryId;

    private long eventId;
    private float seedtime;

    private List<Result> time;

}
