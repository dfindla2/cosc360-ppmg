package au.org.mastersswimmingqld.ppmg.models;

import au.org.mastersswimmingqld.ppmg.enums.Course;
import au.org.mastersswimmingqld.ppmg.enums.Stroke;

/**
 * Handles events in meets
 */
public class Event {

    private long id;

    private Course course;
    private Stroke stroke;
    private int distance;
    private int legs;

    private String program;
    private String name;

    private long session;

}
