package au.org.mastersswimmingqld.ppmg.models;

import java.util.Date;
import java.util.List;

/**
 * Handles details of meets
 *
 */

public class Meet {

    private long id;

    private String name;

    private Date start;
    private Date end;

    List<Session> sessions;
    List<Event> events;

}
