package au.org.mastersswimmingqld.ppmg.models;

import java.util.Map;

/**
 * Heat handling class
 */
public class Heat {

    private long id;
    private long meetId;
    private long eventId;

    private int number;

    private Map<Integer, EntryEvent> entrants;

}
