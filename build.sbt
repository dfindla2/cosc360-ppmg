name := """eprogram2"""

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  "org.webjars" % "angularjs" % "1.5.8",
  "org.webjars.bower" % "angular-animate" % "1.5.8",
  "org.webjars.bower" % "angular-touch" % "1.5.8",
  "org.webjars" % "angular-ui-bootstrap" % "1.3.3",
  "org.webjars" % "requirejs" % "2.1.11-1",
  "org.webjars" % "font-awesome" % "4.6.3"
)     

lazy val root = (project in file(".")).enablePlugins(PlayScala)

pipelineStages := Seq(rjs, digest, gzip)
