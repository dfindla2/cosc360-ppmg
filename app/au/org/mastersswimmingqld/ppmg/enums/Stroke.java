package au.org.mastersswimmingqld.ppmg.enums;

/**
 * Created by david on 10/09/2016.
 */
public enum Stroke {
    FS      ("Freestyle", "Free"),
    BR      ("Breaststroke", "Breast"),
    BS      ("Backstroke", "Back"),
    FL      ("Butterfly", "Fly");

    private final String longName;
    private final String shortName;

    Stroke(String longName, String shortName) {
        this.longName = longName;
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public String getShortName() {
        return shortName;
    }
}
