package au.org.mastersswimmingqld.ppmg.models;

import java.util.List;

/**
 * Handles teams for athletes
 *
 */
public class Team {
    private long id;

    private String name;
    private String code;
    private String shortname;

    private List<Athlete> members;

}
