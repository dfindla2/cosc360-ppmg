COSC360 - PPMG Swimming Live Scoring
=================

Web Development Assignment by [David Findlay](mailto:davidjwfindlay@gmail.com)

Project Uses
* [Play Framework](https://www.playframework.com)
* [angular-seed](https://github.com/angular/angular-seed)
* [WebJars](http://www.webjars.org/)
* [requirejs](http://requirejs.org/)
* [Typesafe Activator Template](http://typesafe.com/activator/template/angular-seed-play).
* [AngularJS](https://angularjs.org/)
* [HTML 5 Boilerplate](https://html5boilerplate.com/)


