package au.org.mastersswimmingqld.ppmg.models;

import java.time.ZonedDateTime;
import java.util.List;

public class Session {

    private long id;
    private long meet;

    private List<Event> events;

    private ZonedDateTime start;
    private ZonedDateTime end;

    private String program;
    private String name;

}
