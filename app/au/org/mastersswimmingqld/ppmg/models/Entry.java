package au.org.mastersswimmingqld.ppmg.models;

import java.util.List;

/**
 * Handles entries to meets for athletes
 *
 */
public class Entry {

    private long id;

    private long meet;
    private long team;
    private long athlete;

    List<EntryEvent> events;

}
