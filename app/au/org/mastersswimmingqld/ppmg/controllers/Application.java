package au.org.mastersswimmingqld.ppmg.controllers;

import play.*;
import play.mvc.*;

/**
 * Created by david on 8/09/2016.
 */
public class Application extends Controller {

    public static Result index() {
        return (ok(views.html.Index.render()));
    }

}
