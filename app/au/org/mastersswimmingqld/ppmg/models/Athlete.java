package au.org.mastersswimmingqld.ppmg.models;

import java.util.Date;
import java.util.List;

/**
 * Handles details of individual athletes
 *
 *
 */
public class Athlete {

    private long id;

    private String firstname;
    private String middlenames;
    private String lastname;
    private String number;
    private Date dob;

    private List<Team> teams;
    private List<Entry> entries;

}
